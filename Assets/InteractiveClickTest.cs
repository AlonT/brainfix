﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveClickTest : Test
{
    override public bool BeginTestReturnEvaluation()
    {
        return UnityEngine.Random.Range(0, 1) == 0;
    }
    
    override public bool Ended()
    {
        Debug.Log("TEST: " + Input.GetMouseButton(0));
        return Input.GetMouseButton(0);
    }
}
