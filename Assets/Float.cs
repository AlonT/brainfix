﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Float : MonoBehaviour
{
    public float speed = 10;
    public float range = 1;
    public GameObject one, two, brain;
    int count = 0;

    void Update()
    {
        brain.transform.rotation = Quaternion.Euler(0, speed * Time.time, 0);
        brain.transform.position = new Vector3(brain.transform.position.x, range * Mathf.Sin(Time.time), 0);
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            if (count == 1)
                SceneManager.LoadScene(1);
            else if(count == 0)
            {
                one.SetActive(false);
                two.SetActive(true);
                brain.SetActive(false);
                count++;
            }
        }

    }
}
