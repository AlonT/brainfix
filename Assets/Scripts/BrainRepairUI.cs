﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BrainRepairUI : MonoBehaviour
{
    // private string nextTestScene = "_Throw_Tester";
    // private string nextTestScene = "Level1";
    
    [SerializeField]
    private GameObject brain;

    [SerializeField]
    private Button startTestsButton;

    public Transform glitches;
    public float glitchSpawnRate = 4f;
    [Range(0,1)]
    public float glitchSpawnChance = 0.5f;
    public GameObject glitchPrefab;

    private float _sensitivity;
    private Vector3 _mouseReference;
    private Vector3 _mouseOffset;
    private Vector3 _rotation;
    private bool _isRotating;
    public SoundFX soundFx;
    public AudioSource audioSrc;
    public GameObject particles;
    public int amountOfGlitches = 0;
    void Start ()
    {
        BrainAttributes.brain = new Dictionary<BrainPartsEnum, float>();

        startTestsButton.onClick.AddListener(StartTests);

        _sensitivity = 0.4f;
        _rotation = Vector3.zero;
        InvokeRepeating("SpawnGlitch", 1, glitchSpawnRate);
    }

    private void StartTests()
    {
        // SceneManager.LoadScene(nextTestScene);
        SceneManager.LoadScene("Level" + TestSubject.lvl);
    }

    void Update()
    {
        var newBrainPos = brain.transform.localPosition;
        float mouseRatioY = Input.mousePosition.y / Screen.height;
        mouseRatioY = Mathf.Clamp01(1-mouseRatioY);
        newBrainPos.y = mouseRatioY * 0.5f;
        brain.transform.localPosition = newBrainPos;
        BrainAttributes.tumors = amountOfGlitches;

        if (_isRotating)
        {
            // offset
            _mouseOffset = (Input.mousePosition - _mouseReference);
             
            // apply rotation
            // _rotation.y = -(_mouseOffset.x + _mouseOffset.y) * _sensitivity;
            
            _rotation.y = -(_mouseOffset.x) * _sensitivity;

            // _rotation.y = (Input.mousePosition.x) * 60f;
            // _rotation.x = (Input.mousePosition.y) * 60f;

            // rotate
            brain.transform.Rotate(_rotation);

            // store mouse
            _mouseReference = Input.mousePosition;
        }

        if (Input.GetMouseButtonDown(0)) {
            MouseDown();
        }
        if (Input.GetMouseButtonUp(0)) {
            MouseUp();
        }
    }
    
    private void MouseDown()
    {
        // rotating flag
        _isRotating = true;
        
        // store mouse
        _mouseReference = Input.mousePosition;
    }
    
    private void MouseUp()
    {
        // rotating flag
        _isRotating = false;
    }

    public void SpawnGlitch()
    {
        GameObject go;
        float v = UnityEngine.Random.Range(0f, 1f);
        int i;
        if(v > glitchSpawnChance)
        {
            i = UnityEngine.Random.Range(0, glitches.childCount);
            if (!glitches.GetChild(i).GetComponentInChildren<GlitchDot>())
            {
                go = Instantiate(glitchPrefab);
                go.transform.SetParent(glitches.GetChild(i));
                go.transform.position = glitches.GetChild(i).position;
                amountOfGlitches++;
            }

        }
    }

    public void PlaySoundEffect(Vector3 pos)
    {
        // PLAY SOUND
        audioSrc.clip = soundFx.clips[UnityEngine.Random.Range(0, soundFx.clips.Length)];
        audioSrc.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
        audioSrc.Play();
        GameObject p = Instantiate(particles, pos, Quaternion.identity);
        Destroy(p, 3f);
    }
}
