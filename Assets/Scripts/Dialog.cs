﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public class Dialog
{
    public TMP_Text text;
    public List<DialogLine> dialog;
    public Color NPCColor = Color.white, playerColor = Color.yellow;
    private int i = 0;

    //Returns false if reached the end of dialog
    public bool NextDialog()
    {
        i++;
        if (i >= dialog.Count) return false;
        text.text = dialog[i].text;
        if (dialog[i].speaker == Speaker.NPC)
        {
            text.color = NPCColor;
        }
        else
            text.color = playerColor;
        return true;
    }

    public void StartDialog()
    {
        text.text = dialog[0].text;
    }
    
}


public enum Speaker
{
    TestSubject, NPC
}
[System.Serializable]
public struct DialogLine
{
    public string text;
    public Speaker speaker;
}