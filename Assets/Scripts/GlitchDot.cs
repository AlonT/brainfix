﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchDot : MonoBehaviour
{
    public SoundFX soundFx;
    public AudioSource audioSrc;
    Vector3 posOnDown;
    Animator anim;
    private void OnMouseDown()
    {
        posOnDown = transform.position;
    }

    private void OnMouseUp()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("Mouse down on glitch");
            FindObjectOfType<BrainRepairUI>().amountOfGlitches--;
            anim = FindObjectOfType<Animator>();
            anim.SetTrigger("End");
            // PLAY SOUND
            PlaySoundEffect();
            Destroy(gameObject, 0.45f);
        }
        //if (posOnDown == transform.position)
        //{
            
        //}
    }

    public void PlaySoundEffect()
    {
        // PLAY SOUND
        audioSrc.clip = soundFx.clips[UnityEngine.Random.Range(0, soundFx.clips.Length)];
        audioSrc.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
        audioSrc.Play();
    }
}
