﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TestSubject : MonoBehaviour
{
    static public int lvl = 1;
    static public int[] neededScoreForLvl = new int[]
    {
        -9999,
        2,
        2,
        4,
        5,
        4,
    };

    [SerializeField]
    private float reachWaypointThreshold;

    // [SerializeField]
    // private float moveSpeed = 0.004f;
    private float moveSpeed = 0.8f;

    // [SerializeField]
    private float lookTargetSpeed = 1;

    [SerializeField]
    private Transform waypointsHolder;
    
    [SerializeField]
    public Transform handHoldPoint;
    
    [SerializeField]
    private Transform lookTarget;

    // [SerializeField]
    private GameObject insaneText;

    // [SerializeField]
    private Image insaneImage;

    // [SerializeField]
    private GameObject headache;
    
    // [SerializeField]
    private TMP_Text scoreNeeded;

    private Transform[] waypointsGameObjects;
    private Waypoint[] waypoints;

    // Status on waypoints and tests
    private int waypoint;
    private int lastStartedTest = -1;
    private bool currentTestEnded;

    private Dictionary<int, int> score;

    private float delayToWaitUntilMoveToNextTarget = -1;

    //--------------
    // These two are Meant to be increased by the tests as the tests are started
    //
    [HideInInspector]
    public float accumulatedFrustration;
    [HideInInspector]
    public float accumulatedExperience;
    //--------------

    public static TestSubject inst;
    
    private float insanity;


    // Start is called before the first frame update
    void Start()
    {
        inst = this;

        insaneText = GameObject.Find("insane-text");
        insaneImage = GameObject.Find("insane-image").GetComponent<Image>();
        insaneText.gameObject.SetActive(false);
        insaneImage.gameObject.SetActive(false);
        
        headache = GameObject.Find("headache");
        scoreNeeded = GameObject.Find("required-score").GetComponent<TMP_Text>();


        // GameObject.Find("ScoreText").GetComponent<TMP_Text>().text = "(Score required: "+neededScoreForLvl[lvl]+")";

        scoreNeeded.text = "(Score required: " + neededScoreForLvl[lvl] + ")";

        score = new Dictionary<int, int>();

        waypointsGameObjects = new Transform[waypointsHolder.childCount];
        waypoints = new Waypoint[waypointsHolder.childCount];
        for (int i = 0; i < waypointsGameObjects.Length; i++) {
            waypointsGameObjects[i] = waypointsHolder.GetChild(i);
                waypoints[i] = waypointsGameObjects[i].GetComponent<Waypoint>();
        }

        waypoint = 0;

        // Hide waypoints
        foreach (var r in waypointsHolder.GetComponentsInChildren<Renderer>()) {
            r.enabled = false;
        }
    }

    private int CurrentTarget()
    {
        return waypoint + 1;
    }
    
    // Update is called once per frame
    void Update()
    {
        
        float fistLevel;
        fistLevel = -0.1f;
        fistLevel += (0.7f * (1f - BrainAttributes.Get(BrainPartsEnum.AggressiveDocile)));
        fistLevel += (0.5f * (Manager.inst.frustration));
        fistLevel = Mathf.Clamp01(fistLevel);
        GetComponentInChildren<Animator>()
            .Play("fist", 0, fistLevel);

        headache.SetActive(BrainAttributes.tumors > 0);
        
        if (insaneText.gameObject.activeSelf) {
            insanity += 0.003f;
            insaneImage.color = new Color(1, 0, 0, insanity);
            if (insanity > 2.8f) {
                UnityEngine.SceneManagement.SceneManager.LoadScene("BrainRepair");
            }
        }
            
        if (waypoint == waypoints.Length - 1 || waypoints[waypoint].isEnd) {
            // Test ends here. Move to next level
            if (Manager.inst.score >= neededScoreForLvl[lvl]) {
                lvl++;
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene("BrainRepair");
            // transform.position = new Vector3(9999f, 9999f, 9999f);
            return;
        }
            
        // waypoint
        var origin = waypointsGameObjects[waypoint].position;
        var target = waypointsGameObjects[CurrentTarget()].position;

        // Transform nextTarget;
        // if (waypoint > waypointsGameObjects.Length - 2) {
        //     nextTarget = waypointsGameObjects[waypointsGameObjects.Length - 1];
        // }
        // else nextTarget = waypointsGameObjects[waypoint + 2];
        // var targetForLook = Vector3.Lerp(waypointsGameObjects[waypoint+1].position, nextTarget.position, 0.05f);

        // move self
        var movement = (target - transform.position).normalized * moveSpeed * Time.deltaTime;
        if (DistToTarget(transform.position += movement, target) <= reachWaypointThreshold) {
            transform.position = target;
        } else {
            transform.position += movement;
        }

        // move looktarget
        // var targetForLook = target;
        // if (waypoints[waypoint+1].lookTarget != null) {
        // targetForLook = waypoints[CurrentTarget()].lookTarget.position;
        // }
        
        var targetForLook = waypoints[CurrentTarget()].lookTarget.position;
        lookTarget.position = Vector3.Lerp(lookTarget.position, targetForLook, lookTargetSpeed * Time.deltaTime);

        // look at looktarget
        transform.LookAt(lookTarget);

        // seek next waypoint
        // "NextItem" means item at target means [waypoint+1]
        var reachedNextItem = DistToTarget(transform.position, target) <= reachWaypointThreshold;
        if (reachedNextItem) {
            var moveOnToNextTarget = false;
            GameObject testGO = waypoints[CurrentTarget()].GetTestGO();
            ITest currentTest = waypoints[CurrentTarget()].GetTest();
            if (currentTest == null) {
                // No test. just keep going
                moveOnToNextTarget = true;
                // This means no delay:
                delayToWaitUntilMoveToNextTarget = -1f;
            } else {
                // Has test
                
                // Has test and hasn't started it
                if (lastStartedTest < waypoint) {
                    lastStartedTest = waypoint;
                    accumulatedFrustration = 0;
                    accumulatedExperience = 0;

                    
                    //sound
                    var audio = testGO.GetComponent<AudioSource>();
                    if (audio != null) {
                        audio.Play();
                    }
                    
                    
                    var secceededThisTest = currentTest.BeginTestReturnEvaluation();
                    Debug.Log("Activating a test called: " + currentTest.TestName());
                    Debug.Log("Succeeded this test? " + secceededThisTest);
                    if (secceededThisTest) {
                        Manager.inst.IncreaseScore(1);
                    } else {
                        Manager.inst.IncreaseScore(0);
                        //always frustrating to lose at anything
                        accumulatedFrustration += 0.3f;
                    }
                    accumulatedExperience += 0.22f;

                    accumulatedFrustration += 0.08f * BrainAttributes.tumors;

                    // accumulatedFrustration = 0.9f;
                    // accumulatedExperience = 0.5f;

                    Manager.inst.IncreaseFrustration(accumulatedFrustration);
                    Manager.inst.IncreaseExperience(accumulatedExperience);

                    if (Manager.inst.frustration >= 1) {
                        GoInsane();
                    }
                }
                
                // Debug.Log("Check for Ended");
                if (currentTest.Ended()) {
                    moveOnToNextTarget = true;
                    
                    // Setup delays after tests
                    if (!currentTestEnded) {
                        currentTestEnded = true;
                        
                        // Purpose is to set delay from each test in inspector
                        delayToWaitUntilMoveToNextTarget = currentTest.DelayAfterEnd();
                    }
                }
            }

            if (moveOnToNextTarget) {
                delayToWaitUntilMoveToNextTarget -= Time.deltaTime;
                if (delayToWaitUntilMoveToNextTarget <= 0) {
                    Debug.Log("Move on to next waypoint");
                    int newWaypoint = -1;
                    if (waypoints[waypoint].splitPath != null && waypoints[waypoint].splitPath.Length > 0) {
                        // FOR LATER SPLITTING PARTS
                        // newWaypoint = ????
                    } else {
                        Debug.Log("Incrementing waypoint");
                        newWaypoint = CurrentTarget();
                    }
                    
                    // Actually progress state:
                    waypoint = newWaypoint;
                    currentTestEnded = false;

                    // Free up the test if any
                    if (currentTest != null) {
                        currentTest.Free();
                    }
                }
            }
        }
    }

    private void GoInsane()
    {
        insaneText.gameObject.SetActive(true);
        insaneImage.gameObject.SetActive(true);
    }

    private float DistToTarget(Vector3 a, Vector3 b)
    {
        return (a - b).magnitude;
    }
}
