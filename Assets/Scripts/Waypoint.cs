﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Waypoint : MonoBehaviour
{
    [HideInInspector]
    public Transform lookTarget;
    
    public Test test;

    public bool isEnd;

    //For later on if we want non linear paths
    public int[] splitPath;

    private void Start()
    {
        // if (lookTarget == null)
        // {
            // var lookTarget = transform.Find("LookTarget");
            var lookTargets = GetComponentsInChildren<Transform>()
                .Where(t => t.gameObject.name.Contains("LookTarget"));
            
            // if (lookTargets.Any()) {
            lookTarget = lookTargets.First();
            // }
        // }
        
        if (test == null) {
            test = GetComponentInChildren<Test>();
        }
    }

    public ITest GetTest()
    {
        return (ITest)test;
    }

    public GameObject GetTestGO()
    {
        if (test == null) {
            return null;
        } else {
            return test.gameObject;
        }
    }
}
