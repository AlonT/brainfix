﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class StatsMenu : MonoBehaviour
{
    public TMP_Text analysisText, challengeText;
    public string[] aggressive = { "is very aggressive", "is clearly assertive", "seems opportunistic", "is careful", "has a docile disposition" };
    public string[] reaction = { "is lightning fast", "is quick", "is perceptive", "is very mindful of his surroundings", "is extremely attentive to details" };
    public string[] adaptation = { "is a fast learner", "is quite adaptive", "is mentally flexible", "is unfased by failure", "has a Happy-Go-Lucky attitude" };
    public string[] challengeDescriptions = {
        "The following challange will test the subject's ability to quickly throw and hit a far away target.",
        "The following challange will require the test subject to find the best item for himself.",
        "The following challenge will demand of the test subject to knock out a siginificant amount of targets.",
        "In the following challenge we will try to trick the test subject.",
        "The following challenge will test the subject's mental stamina and ability to stay focused",
        "We're out of tests for the day. But that's doesn't mean we have to stop perfecting this bastard.",
    };
 
    private BrainRepairUI b;

    private void Start()
    {
        b = FindObjectOfType<BrainRepairUI>();
    }
    private void Update()
    {
        if (BrainAttributes.brain != null) {
            UpdateText();
        }
    }

    public void UpdateText()
    {
        analysisText.text =
            "Subject " +
            GetPartStatus(BrainPartsEnum.AdaptiveResilient) +
            "\nSubject " +
            GetPartStatus(BrainPartsEnum.ReactionAttention) +
            "\nSubject " +
            GetPartStatus(BrainPartsEnum.AggressiveDocile) +
            "\n\n" + GetGlitchState();
        challengeText.text = challengeDescriptions[TestSubject.lvl - 1];
    }
    
    private string GetGlitchState()
    {
        int num = b.amountOfGlitches;
        string s = "The subject is feeling well.";
        switch (num)
        {
            case 1:
                s = "The subject is a bit off.";
                break;
            case 2:
                s = "The subject looks distracted.";
                break;
            case 3:
                s = "The subject is mildly uncomfortable.";
                 break;
            case 4:
                s = "The subject looks tired.";
                break;
            case 5:
                s = "The subject is sick.";
                break;
            case 6:
                s = "The subject is unfit for service.";
                break;
            default:
                break;
        }
        return s;

    }

    public string GetPartStatus(BrainPartsEnum part)
    {
        int i = TraitPercentToInt(BrainAttributes.Get(part));
        switch (part)
        {
            case BrainPartsEnum.AggressiveDocile:
                return aggressive[i];
                break;
            case BrainPartsEnum.ReactionAttention:
                return reaction[i];
                break;
            case BrainPartsEnum.AdaptiveResilient:
                return adaptation[i];
                break;
            default:
                return "";
                break;
        }
    }

    public static int TraitPercentToInt(float v)
    {
        if (v <= 0.2f)
            return 0;
        else if (v > 0.2f && v <= 0.4f)
            return 1;
        else if (v > 0.4f && v <= 0.6f)
            return 2;
        else if (v > 0.6f && v <= 0.8f)
            return 3;
        else
            return 4;
    }

}
