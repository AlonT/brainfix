using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainPartUI : MonoBehaviour
{
    public BrainPartsEnum part;

    [SerializeField]
    [Range(0, 1)]
    private float baseHue;
    
    [Range(0, 1)]
    public float val;
    
    private Vector3 posOnDown;
    BrainRepairUI brainUI;
    void Start ()
    {
        val = UnityEngine.Random.Range(0f, 1f);
        brainUI = FindObjectOfType<BrainRepairUI>();
    }
    
    void Update()
    {
        var color = new Color();
        var hueRange = 0.2f;
        color = Color.HSVToRGB(baseHue + (hueRange * val), 1, 1);
        
        // var renderer = GetComponentInChildren<MeshRenderer>();
        var renderer = transform.GetChild(0).GetComponent<MeshRenderer>();
        
        renderer.material.SetColor("_Color", color);
        
        BrainAttributes.brain[part] = val;
    }
    
    public void OnMouseDown()
    {
        posOnDown = transform.position;
    }

    public void OnMouseUp()
    {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            brainUI.PlaySoundEffect(hit.point);
            val = val + UnityEngine.Random.Range(0.05f, 0.3f);
            val = val % 1f;

        }
        //if (posOnDown == transform.position) {
        // Debug.Log("Mouse down on brain part");
    }
    }

