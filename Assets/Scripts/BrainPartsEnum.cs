public enum BrainPartsEnum
{
    AggressiveDocile  = 0,
    ReactionAttention = 1,
    // TraumaticNaive = 2,
    AdaptiveResilient = 2,
}
