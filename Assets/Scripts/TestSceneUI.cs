﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestSceneUI : MonoBehaviour
{
    // [SerializeField]
    private Button brainRepairButton;

    // Start is called before the first frame update
    void Start()
    {
        brainRepairButton = GameObject.Find("BrainRepairButton").GetComponent<Button>();
        brainRepairButton.onClick.AddListener(BackToBrainRepair);
        if (BrainAttributes.brain != null) {
            Debug.Log("Attributes:");
            Debug.Log("AggressiveDocile:" + BrainAttributes.brain[BrainPartsEnum.AggressiveDocile]);
            Debug.Log("ReactionAttention:" + BrainAttributes.brain[BrainPartsEnum.ReactionAttention]);
            Debug.Log("AdaptiveResilient:" + BrainAttributes.brain[BrainPartsEnum.AdaptiveResilient]);
        }
        
        if (BrainAttributes.brain == null) {
            BackToBrainRepair();
        }
    }

    private void BackToBrainRepair()
    {
        SceneManager.LoadScene("BrainRepair");
    }

    // Update is called once per frame
    void Update()
    {
    }
}
