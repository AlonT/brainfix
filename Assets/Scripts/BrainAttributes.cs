using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainAttributes
{
    static public Dictionary<BrainPartsEnum, float> brain;
    static public Dictionary<IngameAttributes, float> attributes;

    static public int tumors;

    static public float Get(BrainPartsEnum part)
    {
        // Game doesn't make sense unless you first adjust these in brain repair scene
        
        if (brain == null) {
            Debug.LogWarning("WARNING: Uninitialized null brain. Run brain scene repair before running tests.");
            brain = new Dictionary<BrainPartsEnum, float>();
        }
        
        if (brain.ContainsKey(part)) {
            Debug.LogWarning("WARNING: Uninitialized brain attribute. Run brain scene repair before running tests.");
            return brain[part];
        }
        return 0;
    }
    
    static public float Get(IngameAttributes attribute)
    {
        if (Manager.inst == null) {
            return 0;
        }
        
        if (attribute == IngameAttributes.experience) {
            return Manager.inst.experience;
        } else {
            return Manager.inst.frustration;
        }
        
        // // These are meant to be initialized in test scene
        
        // if (attributes == null) {
        //     attributes = new Dictionary<IngameAttributes, float>();
        // }
        
        // if (attributes.ContainsKey(attribute)) {
        //     return attributes[attribute];
        // }
        // return 0;
    }
}
