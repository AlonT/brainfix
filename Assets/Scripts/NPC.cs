﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : Test
{
    public float textSpeed;

    public Canvas canvas;

    [SerializeField]
    private QuestionsEnum[] questions;

    // For when the same NPC is interacted with multiple times.
    private int indexInQuestions;

    public Text testText;
    public Text testSubjectResponse;
    public Text testReplyText;
    
    private Vector3 originLocalPosition;
    private Vector3 targetLocalPosition;
    
    private bool activated;

    private bool finished;

    public float textProgress;

    

    // Start is called before the first frame update
    void Start()
    {
        Reset();
        delayAfterEnd = 6f;
    }

    private void SetCanvas(bool val)
    {
        canvas.gameObject.SetActive(val);
    }

    // Update is called once per frame
    void Update()
    {
        if (activated) {
            textProgress += Time.deltaTime * textSpeed;
            testSubjectResponse.transform.localPosition = Vector3.Lerp(originLocalPosition, targetLocalPosition, textProgress);
            // var dist = testSubjectResponse.transform.localPosition - targetLocalPosition;

            // progress color
            testSubjectResponse.color = Color.Lerp(new Color(255, 255, 0, 0), new Color(255, 255, 0, 1), textProgress);

            // if (dist.magnitude < 0.05f) {
            if (textProgress >= 0.97f) {
                testReplyText.gameObject.SetActive(true);
            }
            
            // if (dist.magnitude < 0.01f) {
            if (textProgress >= 1) {
                finished = true;
                activated = false;
            }

            // var groundNPC = transform.position;
            // var groundTetsSubject = TestSubject.inst.transform.position;
            // groundNPC.y = 0;
            // groundTetsSubject.y = 0;
            var oldRot = transform.localRotation;
            transform.LookAt(TestSubject.inst.transform);
            var newRot = transform.localRotation;

            transform.localRotation = Quaternion.Lerp(oldRot, newRot, 0.04f);
        }
    }
    
    override public string TestName()
    {
        return "Guy calling you a loser";
    }
    
    override public bool BeginTestReturnEvaluation()
    {
        SetCanvas(true);
        
        testText.gameObject.SetActive(true);
        // if (!testSubjectResponse.gameObject.active) {
        testSubjectResponse.gameObject.SetActive(true);

        activated = true;
        
        var testSubject = TestSubject.inst;

        targetLocalPosition = testSubjectResponse.transform.localPosition;
        testSubjectResponse.transform.position = testSubject.transform.position;
        originLocalPosition = testSubjectResponse.transform.localPosition;
        // }


        var question = questions[indexInQuestions++];
        var isAnswerGood = Dialogs.SetupQuestion(question, testText, testSubjectResponse, testReplyText);
        return isAnswerGood;
    }
    
    override public bool Ended()
    {
        return finished;
    }

    override public void Free()
    {
        Reset();
    }

    private void Reset()
    {
        activated = false;
        finished = false;
        textProgress = 0;

        testText.gameObject.SetActive(false);
        // if (!testSubjectResponse.gameObject.active) {
        testSubjectResponse.gameObject.SetActive(false);
        testReplyText.gameObject.SetActive(false);
        
        SetCanvas(false);
    }
}
