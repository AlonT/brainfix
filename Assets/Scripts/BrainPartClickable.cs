using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainPartClickable : MonoBehaviour
{
    private BrainPartUI part;

    private void Start()
    {
        part = GetComponentInParent<BrainPartUI>();
    }
    
    private void OnMouseDown()
    {
        Debug.Log("OnMouseDown");
        part.OnMouseDown();
    }
    
    private void OnMouseUp()
    {
        part.OnMouseUp();
    }
}
