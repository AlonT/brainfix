using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Test : MonoBehaviour, ITest
{
    [SerializeField]
    protected float delayAfterEnd;

    virtual public string TestName()
    {
        return "INVALID NAME. Must override 'Test' class methods!";
    }
    
    virtual public bool BeginTestReturnEvaluation()
    {
        return false;
    }
    
    virtual public bool Ended()
    {
        return false;
    }
    
    virtual public float DelayAfterEnd()
    {
        return delayAfterEnd;
    }
    
    virtual public void Free()
    {
    }
}
