﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Manager : MonoBehaviour
{
    public float frustration, experience;
    public Slider frustrationSlider, expSlider;
    public TMP_Text scoreText;
    public GameObject[] hitMarkers;
    public Color fColor, expColor, scoreColor = Color.white;
    private Animator fAnim, expAnim;
    public int score;

    static public Manager inst;

    private void Start()
    {
        inst = this;

        score = 0;
        frustration = 0;
        experience = 0;
        scoreText.text = "Score: " + score;
        frustrationSlider.gameObject.SetActive(false);
        expSlider.gameObject.SetActive(false);
        fAnim = frustrationSlider.GetComponent<Animator>();
        expAnim = expSlider.GetComponent<Animator>();
        frustrationSlider.value = frustration;
        expSlider.value = experience;


        // Added here because it wasnt possible to both eppear and add at the same time
        expSlider.gameObject.SetActive(true);
        // expAnim.SetTrigger("Appear");
        frustrationSlider.gameObject.SetActive(true);
        // fAnim.SetTrigger("Appear");
    }

    public void IncreaseValues(float f, float exp, int score, float delay = 0.35f)
    {
        StartCoroutine(IncreaseAll(f, exp, score, delay));
    }

    private IEnumerator IncreaseAll(float f, float exp, int score, float delay)
    {
        WaitForSeconds w = new WaitForSeconds(delay);
        if(f!=0)
        {
            IncreaseFrustration(f);
            yield return w;
        }
        if (exp != 0)
        {
            IncreaseExperience(exp);
            yield return w;
        }
        if (score != 0)
        {
            IncreaseScore(score);
            yield return w;
        }
    }

    public void IncreaseScore(int s)
    {
        score += s;
        StartCoroutine(SpawnHitMarker(s, "Score", 0));
        scoreText.text = "Score: " + score;
    }
    public void IncreaseFrustration(float v)
    {
        // Brain attribute acts as modifier for frustration
        var resilient = BrainAttributes.Get(BrainPartsEnum.AdaptiveResilient);
        var frustrationVulnerability = 1 - resilient;

        // frustrationVulnerability *= 0.6f;

        v *= frustrationVulnerability;
        // Limit some float digits
        v *= 100;
        v = Mathf.Round(v);
        v /= 100;

        if (v < 0.01f) {
            // Tiny amounts of frustration aren't really meaningful
            return;
        }
        
        frustration += v;
        frustration = Mathf.Clamp01(frustration);
        if (!frustrationSlider.gameObject.activeSelf)
        {
        }
        else
        {
            fAnim.SetTrigger("Add");
            StartCoroutine(AddSliderValue(frustrationSlider, frustrationSlider.value, v));
        }
        StartCoroutine(SpawnHitMarker(v, "Frustration", 2));
        //if (frustration == 0)
        //{
        //    fAnim.SetTrigger("Disappear");
        //    frustrationSlider.value = 0;
        //}
        //frustrationSlider.value = frustration;
    }

    private IEnumerator SpawnHitMarker(float v, string str, int type)
    {
        float delay = 0;
        switch (type) {
            case 0:
                //Score
                delay = 1f;
                break;
            case 1:
                //Experience
                delay = 1.2f;
                break;
            case 2:
                //Frustration
                delay = 1.8f;
                break;
        }

        WaitForSeconds w = new WaitForSeconds(delay);
        yield return w;

        SpawnHitMarkerCoroutine(v, str, type);
    }
    
    private void SpawnHitMarkerCoroutine(float v, string str, int type)
    {
        GameObject go = Instantiate(hitMarkers[type]);
        TMP_Text text = go.GetComponentInChildren<TMP_Text>();

        
        // go
        //     .transform.position
        //     // .GetComponentInChildren<Text>()
        //     // .transform
        //     // .position
        // text.transform.position
        //     += new Vector3(0, offset);
        
        if (v >= 0)
        {
            if (str != "Score")
                text.text = "+" + v * 100 + " " + str;
            else
                text.text = "+" + v + " " + str;
        }
        else
        {
            if (str != "Score")
                text.text = "-" + v * 100 + " " + str;
            else
                text.text = "-" + v + " " + str;
        }
        
        if (str == "Score")
        {    if (v > 0) {
                text.text = "Success";
                text.color = Color.green;
            } else {
                text.text = "Failure";
                text.color = Color.red;
            }
            // text.color = scoreColor;
            Animator anim = go.GetComponent<Animator>();
            anim.SetTrigger("Score");
        }
        else
        {
            if (str == "Frustration")
                text.color = fColor;
            else if (str == "Experience")
                text.color = expColor;
            Animator anim = go.GetComponent<Animator>();
            anim.SetTrigger("Popup");
        }
    }

    public void IncreaseExperience(float v)
    {
        // Brain attribute acts as modifier for frustration

        var adaptive = 1 - BrainAttributes.Get(BrainPartsEnum.AdaptiveResilient);
        
        v *= adaptive;
        // Limit some float digits
        v *= 100;
        v = Mathf.Round(v);
        v /= 100;

        if (v < 0.01f) {
            // Tiny amounts of experie aren't really meaningful
            return;
        }
        
        experience += v;
        experience = Mathf.Clamp01(experience);
        if (!expSlider.gameObject.activeSelf)
        {
        }
        else
        {
            expAnim.SetTrigger("Add");
            StartCoroutine(AddSliderValue(expSlider, expSlider.value, v));
        }
        StartCoroutine(SpawnHitMarker(v, "Experience", 1));
        //if (experience == 0)
        //{
        //    expAnim.SetTrigger("Disappear");
        //    expSlider.value = 0;
        //}
    }

    private IEnumerator AddSliderValue(Slider s, float currentVal, float valToAdd)
    {
        int sign;
        float final = currentVal + valToAdd;
        if (final >= currentVal) sign = 1;
        else
        {
            sign = -1;
            valToAdd *= sign;
        }
        while(valToAdd > 0)
        {
            s.value += 0.05f;
            valToAdd -= 0.05f;
            yield return null;
        }

        s.value = final;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.V))
        {
            IncreaseValues(0.05f, 0.04f, 1);
        }
    }
}




