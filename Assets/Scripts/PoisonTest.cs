using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoisonTest : Test
{
    private bool succeed = false;

    [SerializeField]
    private float skillRequired;
    
    [SerializeField]
    private float speed;

    private Vector3 startPos;
    private Vector3 endPos;

    private float progress;

    public GameObject goodDrink;
    public GameObject badPoison;

    private bool active;
    private bool finished;

    private GameObject drinkChosen;

    public void Start()
    {
    }

    override public string TestName()
    {
        return "Drink the good drink";
    }
    
    override public bool BeginTestReturnEvaluation ()
    {
        // return UnityEngine.Random.RandomRange(0, 4) < 2;

        //TODO RENAME TRHWO TO POISON

        Debug.Log("POISON TEST");
        
        var succeed = false;
        var attentionExperience = BrainAttributes.Get(IngameAttributes.experience);
        var attention = BrainAttributes.Get(BrainPartsEnum.ReactionAttention);
        var totalThrowSkill = attention + (0.6f*attentionExperience);

        succeed = totalThrowSkill >= skillRequired;


        if (succeed) {
            drinkChosen = goodDrink;
        } else {
            drinkChosen = badPoison;
        }

        startPos = drinkChosen.transform.position;
        endPos = TestSubject.inst.handHoldPoint.position;
        active = true;
        
        return succeed;
    }

    private void Update()
    {
        if (active) {
            progress += Time.deltaTime * speed;
            if (progress >= 1) {
                progress = 1;
                active = false;
                finished = true;
                drinkChosen.transform.localScale *= 0.001f;
            }
            drinkChosen.transform.position =
                Vector3.LerpUnclamped(startPos, endPos, progress);
        }
    }
    
    override public bool Ended()
    {
        return finished;
    }
}
