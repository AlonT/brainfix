﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITest
{
    string TestName();
    bool BeginTestReturnEvaluation();
    bool Ended();
    float DelayAfterEnd();
    void Free();
}
