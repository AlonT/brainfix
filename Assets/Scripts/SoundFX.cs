﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SoundFX")]
public class SoundFX : ScriptableObject
{
    public AudioClip[] clips;
}
