public enum QuestionsEnum
{
    HowOldAreYou = 0,
    HowOldAreYouRepeat = 1,
    WhereWereYouBorn = 2,
    WhereWereYouWhenTheFinalWarEnded = 3,
    WhatIsYourFavoriteColor = 4,
    WhatIsYourFavoriteColorRepeat = 5,
    HowDoYouFeel = 6,
    IsThisChallengingToYou = 7,
    WhatDoYouThinkIsMyAge = 8,
    DoYouTrustMeAsADoctor = 9,
}
