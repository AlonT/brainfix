using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ThrowTest : Test
{
    private bool succeed = false;

    [SerializeField]
    private float throwSkillRequired;
    
    [SerializeField]
    private float speed;
    [SerializeField]
    private Transform hitPoint;
    [SerializeField]
    private Transform missPoint;

    private Vector3 startPos;
    private Vector3 endPos;

    private float progress;

    public GameObject[] balls;

    private bool active;
    private bool finished;
    
    private GameObject ballThrown;

    public void Start()
    {
    }

    override public string TestName()
    {
        return "Throw a ball";
    }
    
    override public bool BeginTestReturnEvaluation ()
    {
        // return UnityEngine.Random.RandomRange(0, 4) < 2;

        var succeed = false;
        var throwExperience = BrainAttributes.Get(IngameAttributes.experience);
        var reaction = 1 - BrainAttributes.Get(BrainPartsEnum.ReactionAttention);
        var aggressive = 1 - BrainAttributes.Get(BrainPartsEnum.AggressiveDocile);
        var totalThrowSkill = reaction + (0.6f*throwExperience) + (0.5f*aggressive);

        //Agressive fist-man throws it harder?
        speed += (0.5f * aggressive);


        succeed = totalThrowSkill >= throwSkillRequired;


        // always do this for throw
        startPos = TestSubject.inst.handHoldPoint.position;
        active = true;

        ballThrown = balls.First();

        if (succeed) {
            endPos = hitPoint.position;
        } else {
            endPos = missPoint.position;
        }
        
        return succeed;
    }

    private void Update()
    {
        if (active) {
            progress += Time.deltaTime * speed;
            if (progress >= 1) {
                progress = 1;
                active = false;
                finished = true;
            }
            ballThrown.transform.position =
                Vector3.LerpUnclamped(startPos, endPos, progress);
        }
    }
    
    override public bool Ended()
    {
        return finished;
    }
}
