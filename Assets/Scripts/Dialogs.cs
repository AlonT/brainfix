using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogs
{
    // static private string howOldMemory;

    static public bool SetupQuestion(QuestionsEnum question, Text textQuestion, Text textAnswer, Text textReply)
    {
        switch (question) {
            case QuestionsEnum.HowOldAreYou:
                textQuestion.text = "How old are you?";

                if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "None of your business!";
                    textReply.text = "As you wish. You may continue.";
                    // howOldMemory = "nosay";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.35f) {
                    textAnswer.text = "32.";
                    textReply.text = "Correct. You may continue.";
                    // howOldMemory = "say";
                    return true;
                } else {
                    textAnswer.text = "I'm... I'm not sure";
                    textReply.text = "That's unfortunate. You may continue.";
                    // howOldMemory = "nosay";
                    return false;
                }
            case QuestionsEnum.HowOldAreYouRepeat:
                textQuestion.text = "How old are you?";
                
                // Just cause it's an annoying to be asked twice
                TestSubject.inst.accumulatedFrustration += 0.1f;
                
                if (Manager.inst.experience > 0.9f) {
                    textAnswer.text = "Didn't you ask me that already, doctor?";
                    textReply.text = "I did. Very good. You may continue.";
                    return true;
                } if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "None of your business!";
                    textReply.text = "I see. How very disappointing. You may continue.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.5f) {
                    textAnswer.text = "32.";
                    textReply.text = "Still correct. You may continue.";
                    return true;
                } else {
                    textAnswer.text = "I'm... I'm not sure.";
                    textReply.text = "Very, very unfortunate. You may continue.";
                    return false;
                }
            case QuestionsEnum.WhereWereYouBorn:
                textQuestion.text = "Where were you born?";

                if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Who cares?!";
                    textReply.text = "We do, as you should. Please continue with the tests.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.5f) {
                    textAnswer.text = "On a battlefield, shortly before the end of the Final War and the founding of Great Utopia.";
                    textReply.text = "Correct. You may continue.";
                    return true;
                } else {
                    textAnswer.text = "I... I don't remember.";
                    textReply.text = "That's unfortunate. You may continue.";
                    return false;
                }
            case QuestionsEnum.WhereWereYouWhenTheFinalWarEnded:
                textQuestion.text = "Where were you when the Final War ended?";

                if (Manager.inst.experience > 0.9f) {
                    textAnswer.text = "It's coming back to me. I feel uncomfortable continuing this conversation, Doctor.";
                    textReply.text = "I see. That is unfortunate. You may continue.";
                    return false;
                } if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Who cares?!";
                    textReply.text = "Apparently no one... Please continue with the tests.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.5f) {
                    textAnswer.text = "In the shelter of the Great Leaders. They are the ones who brought me here, to the CC lab.";
                    textReply.text = "Excellent. You may continue.";
                    return true;
                } else {
                    textAnswer.text = " I... I don't remember.";
                    textReply.text = "That's unfortunate. You may continue.";
                    return false;
                }
            case QuestionsEnum.WhatIsYourFavoriteColor:
                textQuestion.text = "What is your favorite color?";

                if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.35f) {
                    textAnswer.text = "Red!";
                    textReply.text = "Thank you for your cooperation. You may continue.";
                    return true;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) < 0.15f) {
                    textAnswer.text = "I don't really care about colors.";
                    textReply.text = "If you say so. You may continue.";
                    return false;
                } else {
                    textAnswer.text = "Teal";
                    textReply.text = "Thank you for your cooperation. You may continue.";
                    return true;
                }
            case QuestionsEnum.WhatIsYourFavoriteColorRepeat:
                textQuestion.text = "What is your favorite color?";

                // Just cause it's an annoying to be asked twice
                TestSubject.inst.accumulatedFrustration += 0.1f;

                if (Manager.inst.experience > 0.9f) {
                    textAnswer.text = "Didn't you ask me that already, doctor?";
                    textReply.text = "I did. Very good. You may continue.";
                    return true;
                } if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Red!";
                    textReply.text = "And you do not mind answering that a second time? Uncharacteristic and disappointing. You may continue.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) < 0.15f) {
                    textAnswer.text = "I'm sorry. I really don't care about colors.";
                    textReply.text = "Doubly disappointing... You may continue.";
                    return false;
                } else {
                    textAnswer.text = "Teal.";
                    textReply.text = "I am experiencing Deja Vu, but you clearly do not. Unfortunate. You may continue.";
                    return false;
                }
            case QuestionsEnum.HowDoYouFeel:
                textQuestion.text = "How do you feel?";
                
                if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.35f) {
                    textAnswer.text = "Agitated.";
                    textReply.text = "Understandable. You may continue.";
                    return true;
                } else if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) > 0.70f) {
                    textAnswer.text = "I haven't given it much thought...";
                    textReply.text = "I see. You may continue.";
                    return false;
                } else {
                    textAnswer.text = "I feel tired...";
                    textReply.text = "A reasonable side effect to your procedure. Thank you for your cooperation. You may continue.";
                    return true;
                }
            case QuestionsEnum.IsThisChallengingToYou:
                textQuestion.text = "Is this challenging to you?";

                if (Manager.inst.experience > 0.9f) {
                    textAnswer.text = "Less and less as I progress.";
                    textReply.text = "And it rhymes! Very well. I hope it doesn't become too easy. You may continue.";
                    return true;
                } if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Hardly! I rip through this obstacle course!";
                    textReply.text = "That's quite graphic, but I'm glad to hear you are enjoying youself. You may continue.";
                    return true;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) < 0.25f) {
                    textAnswer.text = "At times, but nothing surprises me!";
                    textReply.text = "I'm happy to hear! You may continue.";
                    return true;
                } else {
                    textAnswer.text = "Honestly? It is exhausting.";
                    textReply.text = "I'm sad to hear that. The tests are meant to be challenging, but not too difficult. We'll try to adjust your calibration accordingly in the future. You may continue to the rest of the test.";
                    return false;
                }
            case QuestionsEnum.WhatDoYouThinkIsMyAge:
                textQuestion.text = "What do you think is my age?";

                if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Who cares?!";
                    textReply.text = "Lack of care. Noted. Please continue with the tests.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.75f) {
                    textAnswer.text = "Around 40.";
                    textReply.text = "You are not mistaken. Very good. You may continue.";
                    return true;
                } else {
                    textAnswer.text = "25-ish?";
                    textReply.text = "I am flattered, but you're way off the mark. Oh well. You may continue.";
                    return false;
                }
            case QuestionsEnum.DoYouTrustMeAsADoctor:
                textQuestion.text = "Do you trust me as a doctor?";
                
                 if (BrainAttributes.Get(BrainPartsEnum.AggressiveDocile) < 0.25f) {
                    textAnswer.text = "Hardly... Your incessent pestering is driving me up the walls!";
                    textReply.text = "There's no need to be obscene. Please, continue.";
                    return false;
                } else if (BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.85f) {
                    textAnswer.text = "I notice you keep asking questions about yourself. Wasn't I the test subject?";
                    textReply.text = "Your criticism of our methods is noted. You may continue.";
                    return false;
                } if (Manager.inst.experience > 0.8f || BrainAttributes.Get(BrainPartsEnum.ReactionAttention) > 0.5f) {
                    textAnswer.text = "Of course! You seem to be professional and experienced.";
                    textReply.text = "Thank you so much for saying that! You may continue.";
                    return true;
                } else {
                    textAnswer.text = "I'm not sure... You seem young and inexperienced.";
                    textReply.text = "Such a backhanded compliment. Continue, then.";
                    return false;
                }
        }
        
        // ERROR
        return false;
    }
}
